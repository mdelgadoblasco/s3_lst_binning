# SNAP Sentinel-3 demo: Satellite data manipulation using SNAP within Jupyter Notebook
## Sentinel-3 Level-2 LST binning

Run the application from Binder clicking to the logo here:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mdelgadoblasco%2Fs3_lst_binning/master?urlpath=lab)

This has been created by Earth Observation Platform & Phi-Lab Engineering Support team (PLES team).
Any enquiry send an email to : PLES_management@rheagroup.com

This repository is licensed with : **[European Space Agency Community License – v2.4 Permissive](https://essr.esa.int/license/european-space-agency-community-license-v2-4-permissive)**

The notebook does the binning of Land Surface Temperature of Sentinel-3 SLSTR Level-2 products that can be defined within the notebook.

Launch the application from the logo above and enjoy!